import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter003/devaloper.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';



class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;
  // text fields' controllers
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _typeController = TextEditingController();


  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _products =
  FirebaseFirestore.instance.collection('products');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
      _descriptionController.text = documentSnapshot['description'];
      _typeController.text = documentSnapshot['type'];


    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _priceController,
                  decoration: const InputDecoration(
                    labelText: 'Price',
                  ),
                ),
                TextField(
                  controller:_descriptionController,
                  decoration: const InputDecoration(labelText: 'Description'),
                ),
                TextField(
                  controller: _typeController,
                  decoration: const InputDecoration(labelText: 'Type'),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String name = _nameController.text;
                    final String description = _descriptionController.text;
                    final String type = _typeController.text;
                    final double price = double.tryParse(_priceController.text);
                    if (name != null && price != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _products
                            .add({"name": name, "price": price,"description":description,"type":type})
                            .then((value) => print("Product Added"))
                            .catchError((error) =>
                            print("Failed to add product: $error"));
                      }

                      if (action == 'update') {
                        // Update the product
                        await _products
                            .doc(documentSnapshot.id)
                            .update({"name": name, "price": price,"description":description,"type":type})
                            .then((value) => print("Product Updated"))
                            .catchError((error) =>
                            print("Failed to update product: $error"));
                      }

                      // Clear the text fields
                      _nameController.text = '';
                      _priceController.text = '';
                      _descriptionController.text ='';
                      _typeController.text ='';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }
  Future<void> _selsepro([DocumentSnapshot documentSnapshot]) async {
    String action = 'select';
    if (documentSnapshot != null) {
      action = 'select';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
      _descriptionController.text = documentSnapshot['description'];
      _typeController.text = documentSnapshot['type'];


    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Container(

            child: Padding(
              padding: EdgeInsets.only(
                  top: 80,
                  left: 20,
                  right: 20,
                  // prevent the soft keyboard from covering text fields
                  bottom: MediaQuery.of(ctx).viewInsets.bottom + 50),

              child: Container(
                color: Colors.deepPurple[100],
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    IconButton(
                      // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
                        icon: FaIcon(FontAwesomeIcons.cookieBite),
                        onPressed: () { print("Pressed"); }
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(documentSnapshot['name'],style: GoogleFonts.playfairDisplaySc(
                                textStyle: const TextStyle(fontSize: 48)
                            ),),
                          ],
                        ),

                        Padding(
                          padding: const EdgeInsets.only(left: 150),
                          child: Text((documentSnapshot['price'].toString()+' ฿'),style: GoogleFonts.openSans(
                              textStyle: const TextStyle(fontSize: 20)
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 120),
                          child: Text('Type:'+(documentSnapshot['type']),style: GoogleFonts.dancingScript(
                              textStyle: const TextStyle(fontSize: 20)
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text(documentSnapshot['description'],style: GoogleFonts.bellefair(
                              textStyle: const TextStyle(fontSize: 20)
                          ),),
                        ),


                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _products
        .doc(productId)
        .delete()
        .then((value) => print("Product Deleted"))
        .catchError((error) => print("Failed to delete product: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a product')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Menu'),
        centerTitle: true,
        backgroundColor: Colors.indigo,
        brightness: Brightness.dark,

      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: Container(
        child: StreamBuilder(
          stream: _products.snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
            if (streamSnapshot.hasData) {
              return ListView.builder(
                itemCount: streamSnapshot.data.docs.length,
                itemBuilder: (context, index) {
                  final DocumentSnapshot documentSnapshot =
                  streamSnapshot.data.docs[index];
                  return Card(
                    margin: const EdgeInsets.all(10),
                    child: ListTile(
                      tileColor: Colors.indigo[100],
                      title: Text(documentSnapshot['name']),
                      subtitle: Text(documentSnapshot['price'].toString()),
                      trailing: SizedBox(
                        width: 100,

                        child: Row(
                          children: [
                            // Press this button to edit a single product
                            IconButton(
                                icon: FaIcon(FontAwesomeIcons.pencil),
                                onPressed: () =>
                                    _createOrUpdate(documentSnapshot)),
                            // This icon button is used to delete a single product
                            IconButton(
                                icon: FaIcon(FontAwesomeIcons.solidTrashCan),
                                onPressed: () =>
                                    _deleteProduct(documentSnapshot.id)),
                          ],
                        ),
                      ),

                      onTap: ()=> _selsepro(documentSnapshot)
                    ),
                  );
                },
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(backgroundColor: Colors.indigoAccent,
        onPressed: () => _createOrUpdate(),
        child: const FaIcon(FontAwesomeIcons.cookieBite),
      ),


    drawer: Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children:<Widget> [
            Container(
              color: Colors.indigo,

              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top,
              ),
              child: Column(
                children: [
                  SizedBox(height: 25,),
                  CircleAvatar(
                    radius: 52,
                    backgroundImage: NetworkImage(
                      ''
                    ),
                  ),
                  SizedBox(height: 12,),
                  Text(''+ user.email,style: TextStyle(fontSize: 15,color:Colors.white),),
                  SizedBox(height: 15,),
                ],
              ),
            ),
            Container(
              child: Wrap(
                runSpacing: 6,
                children: [
                  ListTile(
                    leading: const FaIcon(FontAwesomeIcons.userSecret),
                    title: const Text('Developer'),
    onTap: () {
    // Navigator.pushNamed(context, '/signup');
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => ProfileDeveloper()));}),
                  const Divider(color: Colors.black54,)
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
  }
  }



