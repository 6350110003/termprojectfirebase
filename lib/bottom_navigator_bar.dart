import 'package:flutter/material.dart';
import 'package:flutter003/chart.dart';
import 'package:flutter003/home.dart';
import 'package:flutter003/user.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class NavigatorBar extends StatefulWidget {
  const NavigatorBar({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<NavigatorBar> createState() => _NavigatorBarState();
}

class _NavigatorBarState extends State<NavigatorBar> {
  List pages =[
    Home(),
    Chart(),
    User(),



  ];
  int currentIndex=0;
  void onTap(int index){
    setState((){
      currentIndex = index;
    });
  }

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body:pages[currentIndex],
      bottomNavigationBar:BottomNavigationBar(
        unselectedFontSize: 0,
        selectedFontSize: 0,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.black,
        onTap: onTap,
        currentIndex: currentIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey.withOpacity(0.5),
        showUnselectedLabels: false,
        showSelectedLabels: false,
        elevation: 0,



        items: const[
          BottomNavigationBarItem( icon: Icon(Icons.home),label:('Home')),
          BottomNavigationBarItem( icon: FaIcon(FontAwesomeIcons.docker),label:('Chart')),
          BottomNavigationBarItem(  icon: FaIcon(FontAwesomeIcons.circleUser),label:('User')),


        ],),


    );
  }
}
