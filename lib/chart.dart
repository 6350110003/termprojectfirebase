import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class Chart extends StatelessWidget {

  List<SalesData> data =[
    SalesData('Jan',35),
    SalesData('Feb',28),
    SalesData('Mar',34),
    SalesData('Apr',32),
    SalesData('May',40),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chart'),
        centerTitle: true,
        backgroundColor: Colors.cyan,
        brightness: Brightness.dark,

      ),
      body: Container(
        padding: EdgeInsets.symmetric(),
        child: SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          title: ChartTitle(text: 'Half Yearly Sales Analysis'),
          legend: Legend(isVisible: true,),
          tooltipBehavior: TooltipBehavior(enable: true),
          series: <ChartSeries<SalesData, String>>[
            LineSeries<SalesData,String>(
                dataSource: data,
                xValueMapper: (SalesData sales,_) => sales.name,
              yValueMapper: (SalesData sales,_) => sales.price,
              name: 'Sales',
              dataLabelSettings: DataLabelSettings(isVisible: true)
            )
          ],
        ),
      ),
    );
  }
}
class SalesData{
  final String name;
  final double price;

  SalesData(this.name, this.price);
}
