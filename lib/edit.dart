import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Edit extends StatefulWidget {
  const Edit({Key key}) : super(key: key);


  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {

  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;


  // text fields' controllers
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _detailsController = TextEditingController();
  final TextEditingController _typeController = TextEditingController();
  final GlobalKey<FormState> _key=new GlobalKey<FormState>();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _products =
  FirebaseFirestore.instance.collection('products');

  Future editProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "name": _nameController.text,
        "price": _priceController.text,
        "details": _detailsController.text,
        "type": _typeController.text

      },
    );
  }

  void _onConfirm(context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Products"),
        backgroundColor: Colors.black,
      ),
      body: Container(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[



            Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20,),
                    Text('Edit your product',style:TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    SizedBox(height: 3,),
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products name',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10,),

                    TextFormField(
                      controller: _priceController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products price',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter price';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                    ),
                    SizedBox(height: 10,),
                    TextFormField(
                      controller: _detailsController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'description',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter description';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _typeController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'description',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter description';
                        }
                        return null;
                      },
                    ),

                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.black)
                        ),
                        onPressed: (){
                          if(_key.currentState.validate()) {
                            _onConfirm(context);
                          }
                        },child:Text('Confirm',))
                  ],

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
