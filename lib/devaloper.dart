import 'dart:io';
import 'package:flutter/material.dart';


class ProfileDeveloper extends StatefulWidget {
  const ProfileDeveloper({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<ProfileDeveloper> createState() => _ProfileDeveloperState();
}

class _ProfileDeveloperState extends State<ProfileDeveloper> {



  @override


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text('System Developer'),

      ),
      body: Container(

        child: Padding(

          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              ListTile(

                leading:CircleAvatar(
                  backgroundImage: AssetImage("asset/294571313_1013661689351612_1356392068584048453_n.jpg",),
                ),
                title: const Text('นางสาวณภัสสรณ์ การเรียบ'),
                subtitle: Text('6350110003@psu.ac.th'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Divider(
                thickness: 0.8,
              ),
              ListTile(

                leading:CircleAvatar(
                  backgroundImage: AssetImage("asset/ma.png",),
                ),
                title: const Text('นางสาว ณัสมา สุภาวรร'),
                subtitle: const Text('6350110006@psu.ac.th'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Divider(
                thickness: 0.8,
              ),
              ListTile(

                leading:CircleAvatar(
                  backgroundImage: AssetImage("asset/291609894_1945475362302293_5349700077767465682_n.jpg",),
                ),
                title: const Text('นางสาว ยลรดี สุขใส'),
                subtitle: const Text('6350110016@psu.ac.th'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Divider(
                thickness: 0.8,
              ),
            ],

          ),
        ),

      ),


    );
  }
}

