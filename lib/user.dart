import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter003/authentication.dart';

import 'package:flutter003/login.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class User extends StatelessWidget {
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('User'),
          centerTitle: true,
          backgroundColor: Colors.indigoAccent,
          brightness: Brightness.dark,
        actions: <Widget>[

          IconButton(
            icon:FaIcon(FontAwesomeIcons.doorOpen,
              color: Colors.white,
              size: 20,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (contex) => Login()),
              ));
            },
          )
        ],
        //systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.black),

      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:  [
            IconButton(
              // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
                icon: FaIcon(FontAwesomeIcons.user),
                onPressed: () { print("Pressed"); }
            ),
            Text('Welcome',
            style: GoogleFonts.dancingScript(
              textStyle: const TextStyle(fontSize: 48)
            ),
              ),
            Text(user.email,
              style: GoogleFonts.unna(
                  textStyle: const TextStyle(fontSize: 25)
              ),
            ),




          ],
        ),
      ),
    );
  }
}
